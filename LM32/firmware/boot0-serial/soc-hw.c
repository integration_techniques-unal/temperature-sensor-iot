#include "soc-hw.h"

uart_t   *uart0  = (uart_t *)   0xF0000000;
timer_t  *timer0 = (timer_t *)  0xF0010000;
gpio_t   *gpio0  = (gpio_t *)   0xF0020000;
control_t *control0 = (control_t *) 0xF0030000;
// uint32_t *sram0  = (uint32_t *) 0x40000000;

uint32_t msec = 0;

/***************************************************************************
 * General utility functions
 */
void sleep(uint32_t msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN | TIMER_IRQEN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void tic_init()
{
	// Setup timer0.0
	timer0->compare0 = (FCPU/1000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

/***************************************************************************
 * UART Functions
 */
void uart_init()
{
	//uart0->ier = 0x00;  // Interrupt Enable Register
	//uart0->lcr = 0x03;  // Line Control Register:    8N1
	//uart0->mcr = 0x00;  // Modem Control Register

	// Setup Divisor register (Fclk / Baud)
	//uart0->div = (FCPU/(57600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr //& UART_DR
	)) ;
	return uart0->rxtx;
}

void uart_putchar(char c)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}

void uart_putstr(char *str)
{
	char *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

/***************************************************************************
 * GPIO Functions
 */
void gpio_lcd_enable()
{
  sleep(50);
  gpio0->out=gpio0->out+0x0200;
  sleep(50);
  gpio0->out=gpio0->out-0x0200;
  sleep(50);
}

void gpio_lcd_init()
{
  gpio0->out=0x000C;  //Display On-Off
  gpio_lcd_enable();
  gpio0->out=0x0038;  //Function set
  gpio_lcd_enable();
  gpio0->out=0x0001;  //Clear Display
  gpio_lcd_enable();
  sleep(1500);
}

void gpio_lcd_blink()
{
  gpio0->out=0x000D;  //Blink on
  gpio_lcd_enable();
}

void gpio_lcd_idle()
{
  gpio0->out=0x0000;
}

void gpio_lcd_welcome()
{
  gpio0->out=gpio_lcd_address(1,1);
  gpio_lcd_enable();
  gpio_lcd_printmsg("T=");
  gpio0->out=gpio_lcd_address(2,1);
  gpio_lcd_enable();
  gpio_lcd_printmsg("R=");

  //gpio0->out=gpio_lcd_address(2,1);
  //gpio_lcd_enable();
  //gpio_lcd_printmsg("Escoja una opcion");
}

void gpio_lcd_measure()
{
	gpio0->out=gpio_lcd_address(2,1);
  	gpio_lcd_enable();
  	
 /* gpio_lcd_printmsg("Desea ingresar nuevo");
  gpio0->out=gpio_lcd_address(2,1);
  gpio_lcd_enable();
  gpio_lcd_printmsg("usuario?");
  gpio0->out=gpio_lcd_address(4,6);
  gpio_lcd_enable();
  gpio_lcd_printmsg("SI      NO");
  gpio0->out=gpio_lcd_address(4,6);
  gpio_lcd_enable();
  gpio_lcd_blink();
*/}

int gpio_lcd_address(int row,int column)
{
  int address;
  switch(row){
    case 1: address = column-1;
            break;
    case 2: address = column-1 + 0x40;
            break;
    case 3: address = column-1 + 0x14;
            break;
    case 4: address = column-1 + 0x54;
            break;
    default: address = 0x00;
  }
  return address+0x80;           
}

void gpio_lcd_printchar(char c)
{
  gpio0->out=gpio_lcd_translate(c);
  gpio_lcd_enable();
}

void gpio_lcd_printmsg(char *str)
{
  char *c = str;
	while(*c) {
		gpio_lcd_printchar(*c);
		c++;
	}
}

int gpio_lcd_translate(char c)
{
  return c+0x0100;
}

/***************************************************************************
 * Control System Functions
 */
uint32_t sensor_get()
{
	return control0 -> sensor_data;
}

void pwm_put(int dutty_c)
{
	control0 -> pwm_out= dutty_c;	
}
