 
#include "soc-hw.h"

int main()
{
	uint32_t datain, datatemp;
	int m, c, d, u;
	int PWM=0, inc=0;
	uint32_t ref=25, dataref, Rd, Ru;
	gpio_lcd_init();
	gpio_lcd_welcome();
	gpio_lcd_idle();
	while(1)
	{	
		uart_putstr("\n\r-----------------");
		datain=sensor_get(); //obtain data from sensor
		
		datatemp=(datain*1000)/17+2000; //sensor data to print
		u=datatemp-(datatemp/10)*10;
		d=(datatemp-(datatemp/100)*100)/10;
		c=(datatemp-(datatemp/1000)*1000)/100;
		m=datatemp/1000;
		
		ref=uart_getchar();  //get reference temperature
		dataref=ref*100;
		Rd=ref/10;	 //convert refernce data to print
		Ru=ref-(Rd*10);
		
		uart_putstr("\n\rTemp = "); //print sensor temp
		uart_putchar(m+48);
		uart_putchar(c+48);
		uart_putchar(',');
		uart_putchar(d+48);
		uart_putchar(u+48);
		uart_putstr(" *C");
		
		uart_putstr("\n\rRef = ");  // print reference
		uart_putchar(Rd+'0');
		uart_putchar(Ru+'0');
		uart_putstr(" *C");
		
		gpio0->out=gpio_lcd_address(1,3);
	  	gpio_lcd_enable();
		gpio_lcd_printchar(m+48);
		gpio_lcd_printchar(c+48);
		gpio_lcd_printchar(',');
		gpio_lcd_printchar(d+48);
		gpio_lcd_printchar(u+48);
		gpio_lcd_printmsg(" *C");
		gpio_lcd_idle();
		
		gpio0->out=gpio_lcd_address(2,3);
	  	gpio_lcd_enable();
		gpio_lcd_printchar(Rd+'0');
		gpio_lcd_printchar(Ru+'0');
		gpio_lcd_printmsg(" *C");
		gpio_lcd_idle();
		
		if(dataref > datatemp) //temperature control
		{
			inc=-10;
		}
		else
		{
			if(dataref < datatemp)
			{
				inc=10;
			}
			else
			{
				inc=0;
			}
		}
		PWM=PWM+inc;
		if(PWM<0)
		{
			PWM=0;
		}
		else
		{
			if(PWM>99)
			{
				PWM=99;
			}
		}
		pwm_put(PWM);
	}
}
