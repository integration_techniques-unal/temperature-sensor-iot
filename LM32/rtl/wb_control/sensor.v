`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:53:57 10/14/2015 
// Design Name: 
// Module Name:    Sensor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sensor( clk, rst, ACK, DataIn, Temp);
	 
input clk;
    input rst;
    input ACK;
    input [7:0] DataIn;
    output [7:0] Temp;
	 reg [7:0] auxDataIn;
	 
	 always @(posedge clk)
	 begin
		if( rst == 0 && ACK == 0 )
		begin
		auxDataIn[7:0] <= DataIn[7:0];
		end
		else 
		begin
		auxDataIn[7:0] <= 8'd0;
		end
	 end
	 
	 assign Temp[7:0] = auxDataIn[7:0];

endmodule
