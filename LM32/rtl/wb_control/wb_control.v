//---------------------------------------------------------------------------
// Temperature Control System and Wishbone Driver
// Based On GPIO
//     0x00	
//
//---------------------------------------------------------------------------

module wb_control (
	input              	clk,
	input              	reset,
	// Wishbone interface
	input              	wb_stb_i,
	input              	wb_cyc_i,
	output             	wb_ack_o,
	input              	wb_we_i,
	input       	[31:0] 	wb_adr_i,
	input        	[3:0] 	wb_sel_i,
	input       	[31:0] 	wb_dat_i,
	output reg  	[31:0] 	wb_dat_o,
	// IO Wires
	input		[7:0]	sensor_in,
	input 			data_flag,
	output wire  		pwm_out
);

//---------------------------------------------------------------------------
// Temperature Sensor
//---------------------------------------------------------------------------

wire 	[7:0] 	temp;

sensor sensor0 (
	.clk	(clk ),
	.rst	(reset ),
	//
	.ACK 	(flag ),
	.DataIn	(sensor_in ),
	.Temp	(temp )
);

//---------------------------------------------------------------------------
// PWM Motor Driver
//---------------------------------------------------------------------------

reg 	[7:0]	dutty_cy;

pwm pwm0 (
	.clk	(clk ),
	//
	.speed_o(pwm_out ),
	.dutty_c(dutty_cy )
);

//---------------------------------------------------------------------------
// Control
//---------------------------------------------------------------------------
/*
reg 	[7:0]	reference;

control control0 (
	.clk	(clk ),
	.rst 	(rst),
	//
	.speed_o(dutty_cy ),
	.reference(reference ),
	.sensor( )
);
*/
//---------------------------------------------------------------------------
// Wishbone
//---------------------------------------------------------------------------

reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk)
begin
	if (reset)
	begin
		ack <= 0;
	end 
	else 
	begin		// Handle WISHBONE access
		ack <= 0;
		if (wb_rd & ~ack)		// read cycle
		begin	
			ack <= 1;
			case (wb_adr_i[7:0])
			'h00: wb_dat_o <= sensor_in;
			'h08: wb_dat_o <= temp;
			default: wb_dat_o <= 32'b0;
			endcase
		end 
		else if (wb_wr & ~ack ) // write cycle 
		begin
			ack <= 1;
			case (wb_adr_i[7:0])
			'h00: begin
			end
			'h04: dutty_cy <= wb_dat_i;
			endcase
		end
	end
end

endmodule
