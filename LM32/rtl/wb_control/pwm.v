module pwm (
	input clk,
	output speed_o,
	input [7:0] dutty_c
	);

reg [7:0] Timer=0;
reg pwm_output;

assign speed_o = pwm_output;

always @(posedge clk)
begin
	if (Timer >= 7'd99)
	begin
		Timer <= 0;
		pwm_output <= pwm_output;
	end
	else
	begin
		Timer <= Timer+1;
		if(Timer >= dutty_c)
		begin
			pwm_output <= 1'b0;
		end
		else
		begin
			pwm_output <= 1'b1;
		end
	end
end

endmodule
