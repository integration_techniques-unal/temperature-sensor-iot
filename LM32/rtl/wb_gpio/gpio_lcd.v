module gpio_lcd (gpio_out,e,rs,db_7,db_6,db_5,db_4,db_3,db_2,db_1,db_0);

input [31:0] gpio_out;
output e;
output rs;
output db_0;
output db_1;
output db_2;
output db_3;
output db_4;
output db_5;
output db_6;
output db_7;

assign {e,rs,db_7,db_6,db_5,db_4,db_3,db_2,db_1,db_0}=~gpio_out[9:0];

endmodule
