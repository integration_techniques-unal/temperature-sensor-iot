#include <stdio.h>
#include <stdlib.h>

int main(){
	
	FILE *archivo;
	FILE *bit1;
	FILE *bit2;
	FILE *bit3;
	FILE *bit4;
	FILE *bit5;
	FILE *bit6;
	FILE *bit7;
	FILE *bit8;	
	FILE *ack;
	
	char caracter;
	char HIGH = '1';
	char LOW = '0';
	int n = 0;
	int m, c, d, u;
	int dato;
	int binario;
	int bit[8]; 
	archivo = fopen("sensor.txt","r");
	bit1 = fopen("/sys/class/gpio/gpio67/value","w");
 	bit2 = fopen("/sys/class/gpio/gpio66/value","w");
 	bit3 = fopen("/sys/class/gpio/gpio68/value","w");
 	bit4 = fopen("/sys/class/gpio/gpio69/value","w");
 	bit5 = fopen("/sys/class/gpio/gpio44/value","w");
 	bit6 = fopen("/sys/class/gpio/gpio45/value","w");
 	bit7 = fopen("/sys/class/gpio/gpio26/value","w");
 	bit8 = fopen("/sys/class/gpio/gpio23/value","w");
 	ack = fopen("/sys/class/gpio/gpio46/value","w");
 	if (archivo == NULL){
		printf("\nError de apertura del archivo. \n\n");
		}else{
			while (feof(archivo) == 0){
			caracter = fgetc(archivo);	
			n = n + 1;
			 switch (n)
			 {
				 case 1:
					 m = caracter - 48;
					 m = m * 1000;  
					 break;
				 case 2:
					 c = caracter - 48;
					 c = c * 100;
					 break;	
				 case 3:
					 d = caracter - 48;
					 d = d * 10;
					 break;	
				 case 4:
					 u = caracter - 48;
					 break;	
				 default:
					 n = 10;
					 if (n == 10){
					 dato = m + c + d + u;
					 printf(" %i mV \n", dato);
					 n = 0;	
					 }
					 m = 0;
					 c = 0;
					 d = 0;
					 u = 0;
					 break;
			 }
				}
				binario = dato - 1239;
				printf(" %i bit \n", binario);
				if( binario >= 0 && binario <= 255 ){
					for(n = 0; n <= 7; n++){
					bit [n] = bin(binario, n);
				}
					}else if( binario >= 256){
						printf(" Por encima del rango \n");
						}else if( binario <= 0 ){
							printf(" Por debajo del rango \n");
							}
			}
			fprintf( ack, "%c", LOW );
			for(n = 0; n<=8; n++){
				switch(n){
					case 0:
					if(bit[n] == 1){
						fprintf( bit1, "%c", HIGH );
						}else{
						fprintf( bit1, "%c", LOW );
							}
					break;
					case 1:
					if(bit[n] == 1){
						fprintf(bit2, "%c", HIGH );
						}else{
						fprintf(bit2, "%c", LOW );
							}
					break;
					case 2:
					if(bit[n] == 1){
						fprintf(bit3, "%c", HIGH );
						}else{
						fprintf(bit3, "%c", LOW );
							}
					break;
					case 3:
					if(bit[n] == 1){
						fprintf(bit4, "%c", HIGH );
						}else{
						fprintf(bit4, "%c", LOW );
							}
					break;
					case 4:
					if(bit[n] == 1){
						fprintf(bit5, "%c", HIGH );
						}else{
						fprintf(bit5, "%c", LOW );
							}
					break;
					case 5:
					if(bit[n] == 1){
						fprintf(bit6, "%c", HIGH );
						}else{
						fprintf(bit6, "%c", LOW );
							}
					break;
					case 6:
					if(bit[n] == 1){
						fprintf(bit7, "%c", HIGH );
						}else{
						fprintf(bit7, "%c", LOW );	
							}
					break;
					case 7:
					if(bit[n] == 1){
						fprintf(bit8, "%c", HIGH );
						}else{
						fprintf(bit8, "%c", LOW );		
							}
					break;
					case 8:
					fprintf( ack, "%c", HIGH );
					break;
					}
				}
	fclose(bit1);
	fclose(bit2);
	fclose(bit3);
	fclose(bit4);
	fclose(bit5);		
	fclose(bit6);
	fclose(bit7);
	fclose(bit8);
	fclose(ack);
	fclose(archivo);
	return 0;
	}

int bin( int b, int pos ){
	
	int n;
	int resultado[8];
	int dato = b;
	
	for( n = 0; n <= 7; n++ ){
		resultado[7 - n] = dato % 2;
		dato = dato / 2;
		}
	return resultado[pos];
	}
